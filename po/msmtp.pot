# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Martin Lambers <marlam@marlam.de>
# This file is distributed under the same license as the msmtp package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: msmtp 1.8.11\n"
"Report-Msgid-Bugs-To: marlam@marlam.de\n"
"POT-Creation-Date: 2020-06-03 21:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. TRANSLATORS: msmtp shares a lot of code and translatable strings with
#. mpop <https://marlam.de/mpop>.
#: src/msmtp.c:89
#, c-format
msgid "%s: FATAL: %s\n"
msgstr ""

#: src/msmtp.c:229 src/msmtp.c:407 src/msmtp.c:1394
#, c-format
msgid "the server does not support TLS via the STARTTLS command"
msgstr ""

#: src/msmtp.c:271
#, c-format
msgid "the server does not support Remote Message Queue Starting"
msgstr ""

#: src/msmtp.c:283 src/msmtp.c:1452
#, c-format
msgid "the server does not support authentication"
msgstr ""

#: src/msmtp.c:442
#, c-format
msgid "%s server at %s (%s [%s]), port %d:\n"
msgstr ""

#: src/msmtp.c:448
#, c-format
msgid "%s server at %s (%s), port %d:\n"
msgstr ""

#: src/msmtp.c:454
#, c-format
msgid "%s server at %s ([%s]), port %d:\n"
msgstr ""

#: src/msmtp.c:460
#, c-format
msgid "%s server at %s, port %d:\n"
msgstr ""

#: src/msmtp.c:480
#, c-format
msgid "No special capabilities.\n"
msgstr ""

#: src/msmtp.c:484
#, c-format
msgid "Capabilities:\n"
msgstr ""

#: src/msmtp.c:488
msgid "Maximum message size is "
msgstr ""

#: src/msmtp.c:491
#, c-format
msgid "unlimited\n"
msgstr ""

#: src/msmtp.c:495
#, c-format
msgid "%ld bytes"
msgstr ""

#: src/msmtp.c:498
#, c-format
msgid " = %.2f MiB"
msgstr ""

#: src/msmtp.c:503
#, c-format
msgid " = %.2f KiB"
msgstr ""

#: src/msmtp.c:510
msgid "Support for command grouping for faster transmission"
msgstr ""

#: src/msmtp.c:515
msgid "Support for RMQS (Remote Message Queue Starting)"
msgstr ""

#: src/msmtp.c:520
msgid "Support for Delivery Status Notifications"
msgstr ""

#: src/msmtp.c:530
msgid "Support for TLS encryption via the STARTTLS command"
msgstr ""

#: src/msmtp.c:536
msgid "Supported authentication methods:"
msgstr ""

#: src/msmtp.c:581
#, c-format
msgid ""
"This server might advertise more or other capabilities when TLS is active.\n"
msgstr ""

#: src/msmtp.c:1146 src/msmtp.c:3282
#, c-format
msgid "cannot write mail headers to temporary file: output error"
msgstr ""

#: src/msmtp.c:1254
#, c-format
msgid "input error while reading the mail"
msgstr ""

#: src/msmtp.c:1441
#, c-format
msgid "the server does not support DSN"
msgstr ""

#: src/msmtp.c:1590 src/msmtp.c:1609 src/msmtp.c:1661
#, c-format
msgid "automatic configuration based on SRV records failed: %s"
msgstr ""

#: src/msmtp.c:1591
msgid "invalid mail address"
msgstr ""

#: src/msmtp.c:1607
#, c-format
msgid "no SRV records for %s or %s"
msgstr ""

#: src/msmtp.c:1624
#, c-format
msgid "copy this to your configuration file %s"
msgstr ""

#: src/msmtp.c:1628
msgid "warning: the host does not match the mail domain; please check"
msgstr ""

#: src/msmtp.c:1631 src/msmtp.c:1635
msgid "add your password to the key ring:"
msgstr ""

#: src/msmtp.c:1638
msgid "encrypt your password:"
msgstr ""

#: src/msmtp.c:1662
msgid "this system lacks libresolv"
msgstr ""

#: src/msmtp.c:1889
#, c-format
msgid "invalid logfile_time_format"
msgstr ""

#: src/msmtp.c:1902
#, c-format
msgid "cannot open: %s"
msgstr ""

#: src/msmtp.c:1910
#, c-format
msgid "cannot lock (tried for %d seconds): %s"
msgstr ""

#: src/msmtp.c:1915
#, c-format
msgid "cannot lock: %s"
msgstr ""

#: src/msmtp.c:1924
msgid "output error"
msgstr ""

#: src/msmtp.c:1941
#, c-format
msgid "cannot log to %s: %s"
msgstr ""

#: src/msmtp.c:1945
#, c-format
msgid "log info was: %s"
msgstr ""

#: src/msmtp.c:2054
#, c-format
msgid "%s version %s\n"
msgstr ""

#: src/msmtp.c:2055
#, c-format
msgid "Platform: %s\n"
msgstr ""

#: src/msmtp.c:2057
#, c-format
msgid "TLS/SSL library: %s\n"
msgstr ""

#: src/msmtp.c:2063 src/msmtp.c:2133
#, c-format
msgid "none"
msgstr ""

#: src/msmtp.c:2067
#, c-format
msgid ""
"Authentication library: %s\n"
"Supported authentication methods:\n"
msgstr ""

#: src/msmtp.c:2070
msgid "GNU SASL; oauthbearer: built-in"
msgstr ""

#: src/msmtp.c:2072
msgid "built-in"
msgstr ""

#: src/msmtp.c:2113
#, c-format
msgid "IDN support: "
msgstr ""

#: src/msmtp.c:2117 src/msmtp.c:2125
#, c-format
msgid "enabled"
msgstr ""

#: src/msmtp.c:2119 src/msmtp.c:2128
#, c-format
msgid "disabled"
msgstr ""

#: src/msmtp.c:2123
#, c-format
msgid "NLS: "
msgstr ""

#: src/msmtp.c:2126
#, c-format
msgid ", LOCALEDIR is %s"
msgstr ""

#: src/msmtp.c:2131
#, c-format
msgid "Keyring support: "
msgstr ""

#: src/msmtp.c:2136
#, c-format
msgid "Gnome "
msgstr ""

#: src/msmtp.c:2139
#, c-format
msgid "MacOS "
msgstr ""

#: src/msmtp.c:2145
#, c-format
msgid "System configuration file name: %s\n"
msgstr ""

#: src/msmtp.c:2149
#, c-format
msgid "User configuration file name: %s\n"
msgstr ""

#: src/msmtp.c:2152
#, c-format
msgid ""
"Copyright (C) 2020 Martin Lambers and others.\n"
"This is free software.  You may redistribute copies of it under the terms "
"of\n"
"the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.\n"
"There is NO WARRANTY, to the extent permitted by law.\n"
msgstr ""

#: src/msmtp.c:2169
#, c-format
msgid ""
"Usage:\n"
"\n"
msgstr ""

#: src/msmtp.c:2170
#, c-format
msgid ""
"Sendmail mode (default):\n"
"  %s [option...] [--] recipient...\n"
"  %s [option...] -t [--] [recipient...]\n"
"  Read a mail from standard input and transmit it to an SMTP or LMTP "
"server.\n"
msgstr ""

#: src/msmtp.c:2175
#, c-format
msgid ""
"Configuration mode:\n"
"  %s --configure=mailadress\n"
"  Generate and print configuration for address.\n"
msgstr ""

#: src/msmtp.c:2178
#, c-format
msgid ""
"Server information mode:\n"
"  %s [option...] --serverinfo\n"
"  Print information about a server.\n"
msgstr ""

#: src/msmtp.c:2181
#, c-format
msgid ""
"Remote Message Queue Starting mode:\n"
"  %s [option...] --rmqs=host|@domain|#queue\n"
"  Send a Remote Message Queue Starting request to a server.\n"
"\n"
msgstr ""

#: src/msmtp.c:2185
#, c-format
msgid "General options:\n"
msgstr ""

#: src/msmtp.c:2186
#, c-format
msgid "  --version                    print version\n"
msgstr ""

#: src/msmtp.c:2187
#, c-format
msgid "  --help                       print help\n"
msgstr ""

#: src/msmtp.c:2188
#, c-format
msgid "  -P, --pretend                print configuration info and exit\n"
msgstr ""

#: src/msmtp.c:2189
#, c-format
msgid "  -d, --debug                  print debugging information\n"
msgstr ""

#: src/msmtp.c:2190
#, c-format
msgid "Changing the mode of operation:\n"
msgstr ""

#: src/msmtp.c:2191
#, c-format
msgid ""
"  --configure=mailaddress      generate and print configuration for address\n"
msgstr ""

#: src/msmtp.c:2192
#, c-format
msgid "  -S, --serverinfo             print information about the server\n"
msgstr ""

#: src/msmtp.c:2193
#, c-format
msgid ""
"  --rmqs=host|@domain|#queue   send a Remote Message Queue Starting request\n"
msgstr ""

#: src/msmtp.c:2194
#, c-format
msgid "Configuration options:\n"
msgstr ""

#: src/msmtp.c:2195
#, c-format
msgid "  -C, --file=filename          set configuration file\n"
msgstr ""

#: src/msmtp.c:2196
#, c-format
msgid ""
"  -a, --account=id             use the given account instead of the account\n"
"                               named \"default\"; its settings may be "
"changed\n"
"                               with command-line options\n"
msgstr ""

#: src/msmtp.c:2199
#, c-format
msgid ""
"  --host=hostname              set the server, use only command-line "
"settings;\n"
"                               do not use any configuration file data\n"
msgstr ""

#: src/msmtp.c:2201
#, c-format
msgid "  --port=number                set port number\n"
msgstr ""

#: src/msmtp.c:2202
#, c-format
msgid ""
"  --source-ip=[IP]             set/unset source ip address to bind the "
"socket to\n"
msgstr ""

#: src/msmtp.c:2203
#, c-format
msgid "  --proxy-host=[IP|hostname]   set/unset proxy\n"
msgstr ""

#: src/msmtp.c:2204
#, c-format
msgid "  --proxy-port=[number]        set/unset proxy port\n"
msgstr ""

#: src/msmtp.c:2205
#, c-format
msgid "  --socket=[socketname]        set/unset local socket to connect to\n"
msgstr ""

#: src/msmtp.c:2206
#, c-format
msgid "  --timeout=(off|seconds)      set/unset network timeout in seconds\n"
msgstr ""

#: src/msmtp.c:2207
#, c-format
msgid "  --protocol=(smtp|lmtp)       use the given sub protocol\n"
msgstr ""

#: src/msmtp.c:2208
#, c-format
msgid ""
"  --domain=string              set the argument of EHLO or LHLO command\n"
msgstr ""

#: src/msmtp.c:2209
#, c-format
msgid ""
"  --auth[=(on|off|method)]     enable/disable authentication and optionally\n"
"                               choose the method\n"
msgstr ""

#: src/msmtp.c:2211
#, c-format
msgid "  --user=[username]            set/unset user name for authentication\n"
msgstr ""

#: src/msmtp.c:2212
#, c-format
msgid "  --passwordeval=[eval]        evaluate password for authentication\n"
msgstr ""

#: src/msmtp.c:2213
#, c-format
msgid "  --tls[=(on|off)]             enable/disable TLS encryption\n"
msgstr ""

#: src/msmtp.c:2214
#, c-format
msgid "  --tls-starttls[=(on|off)]    enable/disable STARTTLS for TLS\n"
msgstr ""

#: src/msmtp.c:2215
#, c-format
msgid "  --tls-trust-file=[file]      set/unset trust file for TLS\n"
msgstr ""

#: src/msmtp.c:2216
#, c-format
msgid "  --tls-crl-file=[file]        set/unset revocation file for TLS\n"
msgstr ""

#: src/msmtp.c:2217
#, c-format
msgid ""
"  --tls-fingerprint=[f]        set/unset trusted certificate fingerprint for "
"TLS\n"
msgstr ""

#: src/msmtp.c:2218
#, c-format
msgid ""
"  --tls-certcheck[=(on|off)]   enable/disable server certificate checks for "
"TLS\n"
msgstr ""

#: src/msmtp.c:2219
#, c-format
msgid "  --tls-key-file=[file]        set/unset private key file for TLS\n"
msgstr ""

#: src/msmtp.c:2220
#, c-format
msgid "  --tls-cert-file=[file]       set/unset private cert file for TLS\n"
msgstr ""

#: src/msmtp.c:2221
#, c-format
msgid "  --tls-priorities=[prios]     set/unset TLS priorities.\n"
msgstr ""

#: src/msmtp.c:2222
#, c-format
msgid ""
"  --tls-host-override=[host]   set/unset override for TLS host "
"verification.\n"
msgstr ""

#: src/msmtp.c:2223
#, c-format
msgid "  --tls-min-dh-prime-bits=[b]  set/unset minimum bit size of DH prime\n"
msgstr ""

#: src/msmtp.c:2224
#, c-format
msgid "Options specific to sendmail mode:\n"
msgstr ""

#: src/msmtp.c:2225
#, c-format
msgid ""
"  --auto-from[=(on|off)]       enable/disable automatic envelope-from "
"addresses\n"
msgstr ""

#: src/msmtp.c:2226
#, c-format
msgid "  -f, --from=address           set envelope from address\n"
msgstr ""

#: src/msmtp.c:2227
#, c-format
msgid ""
"  --maildomain=[domain]        set the domain for automatic envelope from\n"
"                               addresses\n"
msgstr ""

#: src/msmtp.c:2229
#, c-format
msgid "  -N, --dsn-notify=(off|cond)  set/unset DSN conditions\n"
msgstr ""

#: src/msmtp.c:2230
#, c-format
msgid "  -R, --dsn-return=(off|ret)   set/unset DSN amount\n"
msgstr ""

#: src/msmtp.c:2231
#, c-format
msgid "  -X, --logfile=[file]         set/unset log file\n"
msgstr ""

#: src/msmtp.c:2232
#, c-format
msgid ""
"  --logfile-time-format=[fmt]  set/unset log file time format for "
"strftime()\n"
msgstr ""

#: src/msmtp.c:2233
#, c-format
msgid ""
"  --syslog[=(on|off|facility)] enable/disable/configure syslog logging\n"
msgstr ""

#: src/msmtp.c:2234
#, c-format
msgid ""
"  -t, --read-recipients        read additional recipients from the mail\n"
msgstr ""

#: src/msmtp.c:2235
#, c-format
msgid ""
"  --read-envelope-from         read envelope from address from the mail\n"
msgstr ""

#: src/msmtp.c:2236
#, c-format
msgid "  --aliases=[file]             set/unset aliases file\n"
msgstr ""

#: src/msmtp.c:2237
#, c-format
msgid "  --set-from-header[=(auto|on|off)] set From header handling\n"
msgstr ""

#: src/msmtp.c:2238
#, c-format
msgid "  --set-date-header[=(auto|off)] set Date header handling\n"
msgstr ""

#: src/msmtp.c:2239
#, c-format
msgid ""
"  --remove-bcc-headers[=(on|off)] enable/disable removal of Bcc headers\n"
msgstr ""

#: src/msmtp.c:2240
#, c-format
msgid ""
"  --undisclosed-recipients[=(on|off)] enable/disable replacement of To/Cc/"
"Bcc\n"
"                               with To: undisclosed-recipients:;\n"
msgstr ""

#: src/msmtp.c:2242
#, c-format
msgid "  --                           end of options\n"
msgstr ""

#: src/msmtp.c:2243
#, c-format
msgid ""
"Accepted but ignored: -A, -B, -bm, -F, -G, -h, -i, -L, -m, -n, -O, -o, -v\n"
msgstr ""

#: src/msmtp.c:2244
#, c-format
msgid ""
"\n"
"Report bugs to <%s>.\n"
msgstr ""

#: src/msmtp.c:2486 src/msmtp.c:2500
msgid "cannot use both --serverinfo and --rmqs"
msgstr ""

#: src/msmtp.c:2520 src/msmtp.c:2532
msgid "cannot use both --host and --account"
msgstr ""

#: src/msmtp.c:2548 src/msmtp.c:2566 src/msmtp.c:2585 src/msmtp.c:2640
#: src/msmtp.c:2672 src/msmtp.c:2690 src/msmtp.c:2753 src/msmtp.c:2798
#: src/msmtp.c:2820 src/msmtp.c:2866 src/msmtp.c:2891 src/msmtp.c:2910
#: src/msmtp.c:2963 src/msmtp.c:3008 src/msmtp.c:3035 src/msmtp.c:3053
#: src/msmtp.c:3072 src/msmtp.c:3091 src/msmtp.c:3109 src/msmtp.c:3127
#: src/msmtp.c:3204
#, c-format
msgid "invalid argument %s for %s"
msgstr ""

#: src/msmtp.c:2595 src/msmtp.c:3167
msgid "cannot use both --from and --read-envelope-from"
msgstr ""

#: src/msmtp.c:3182
#, c-format
msgid "unsupported operation mode b%s"
msgstr ""

#: src/msmtp.c:3259 src/msmtp.c:3736 src/msmtp.c:4029
#, c-format
msgid "cannot create temporary file: %s"
msgstr ""

#: src/msmtp.c:3289 src/msmtp.c:3761 src/msmtp.c:4062
#, c-format
msgid "cannot rewind temporary file: %s"
msgstr ""

#: src/msmtp.c:3354
#, c-format
msgid "ignoring system configuration file %s: %s\n"
msgstr ""

#: src/msmtp.c:3369
#, c-format
msgid "loaded system configuration file %s\n"
msgstr ""

#: src/msmtp.c:3408
#, c-format
msgid "ignoring user configuration file %s: %s\n"
msgstr ""

#: src/msmtp.c:3423
#, c-format
msgid "loaded user configuration file %s\n"
msgstr ""

#: src/msmtp.c:3480
#, c-format
msgid "using account %s from %s\n"
msgstr ""

#: src/msmtp.c:3486 src/msmtp.c:3488 src/msmtp.c:3491 src/msmtp.c:3525
#: src/msmtp.c:3526 src/msmtp.c:3528 src/msmtp.c:3530 src/msmtp.c:3534
#: src/msmtp.c:3536 src/msmtp.c:3555 src/msmtp.c:3557 src/msmtp.c:3559
#: src/msmtp.c:3569 src/msmtp.c:3572 src/msmtp.c:3574 src/msmtp.c:3579
#: src/msmtp.c:3582 src/msmtp.c:3594 src/msmtp.c:3596 src/msmtp.c:3598
#: src/msmtp.c:3601 src/msmtp.c:3603 src/msmtp.c:3605
msgid "(not set)"
msgstr ""

#: src/msmtp.c:3495
#, c-format
msgid "off\n"
msgstr ""

#: src/msmtp.c:3501
#, c-format
msgid "%d seconds\n"
msgstr ""

#: src/msmtp.c:3505
#, c-format
msgid "1 second\n"
msgstr ""

#: src/msmtp.c:3514
#, c-format
msgid "none\n"
msgstr ""

#: src/msmtp.c:3518
#, c-format
msgid "choose\n"
msgstr ""

#: src/msmtp.c:3531 src/msmtp.c:3532 src/msmtp.c:3561 src/msmtp.c:3577
#: src/msmtp.c:3585 src/msmtp.c:3590 src/msmtp.c:3592
msgid "on"
msgstr ""

#: src/msmtp.c:3531 src/msmtp.c:3532 src/msmtp.c:3561 src/msmtp.c:3577
#: src/msmtp.c:3585 src/msmtp.c:3588 src/msmtp.c:3590 src/msmtp.c:3592
msgid "off"
msgstr ""

#: src/msmtp.c:3582
msgid "(read from mail)"
msgstr ""

#: src/msmtp.c:3584 src/msmtp.c:3587
msgid "auto"
msgstr ""

#: src/msmtp.c:3608
#, c-format
msgid "reading recipients from the command line and the mail\n"
msgstr ""

#: src/msmtp.c:3613
#, c-format
msgid "reading recipients from the command line\n"
msgstr ""

#: src/msmtp.c:3726
msgid "too many arguments"
msgstr ""

#: src/msmtp.c:3755
#, c-format
msgid "envelope from address extracted from mail: %s\n"
msgstr ""

#: src/msmtp.c:3770
msgid "no recipients found"
msgstr ""

#: src/msmtp.c:3800
#, c-format
msgid "account chosen by envelope from address %s: %s\n"
msgstr ""

#: src/msmtp.c:3814
#, c-format
msgid "falling back to default account\n"
msgstr ""

#: src/msmtp.c:3837
#, c-format
msgid "using environment variables EMAIL and SMTPSERVER\n"
msgstr ""

#: src/msmtp.c:3845
#, c-format
msgid "account %s not found in %s and %s"
msgstr ""

#: src/msmtp.c:3851 src/msmtp.c:3856
#, c-format
msgid "account %s not found in %s"
msgstr ""

#: src/msmtp.c:3861
#, c-format
msgid "account %s not found: no configuration file available"
msgstr ""

#: src/msmtp.c:3876
#, c-format
msgid "using account specified on command line\n"
msgstr ""

#: src/msmtp.c:3936
#, c-format
msgid "account %s from %s: %s"
msgstr ""

#: src/msmtp.c:3977
msgid "this platform does not support syslog logging"
msgstr ""

#: src/msmtp.c:3986
#, c-format
msgid "support for authentication method %s is not compiled in"
msgstr ""

#: src/msmtp.c:3994
#, c-format
msgid "cannot initialize networking: %s"
msgstr ""

#: src/msmtp.c:4005
#, c-format
msgid "cannot initialize TLS library: %s"
msgstr ""

#: src/msmtp.c:4012
msgid "support for TLS is not compiled in"
msgstr ""

#: src/msmtp.c:4091
#, c-format
msgid "LMTP server message: %s"
msgstr ""

#: src/msmtp.c:4102
#, c-format
msgid "could not send mail to all recipients (account %s from %s)"
msgstr ""

#: src/msmtp.c:4108
msgid "could not send mail to all recipients"
msgstr ""

#: src/msmtp.c:4123 src/msmtp.c:4178 src/msmtp.c:4199
#, c-format
msgid "server message: %s"
msgstr ""

#: src/msmtp.c:4129
#, c-format
msgid "could not send mail (account %s from %s)"
msgstr ""

#: src/msmtp.c:4134
msgid "could not send mail"
msgstr ""

#: src/msmtp.c:4144
#, c-format
msgid "delivery to one or more recipients failed"
msgstr ""

#: src/conf.c:768 src/conf.c:783
#, c-format
msgid "host not set"
msgstr ""

#: src/conf.c:773
#, c-format
msgid "port not set"
msgstr ""

#: src/conf.c:778
#, c-format
msgid "envelope-from address is missing"
msgstr ""

#: src/conf.c:788
#, c-format
msgid "tls_key_file requires tls_cert_file"
msgstr ""

#: src/conf.c:793
#, c-format
msgid "tls_cert_file requires tls_key_file"
msgstr ""

#: src/conf.c:801
#, c-format
msgid ""
"tls requires either tls_trust_file (highly recommended) or tls_fingerprint "
"or a disabled tls_certcheck"
msgstr ""

#: src/conf.c:807
#, c-format
msgid "tls_crl_file requires tls_trust_file"
msgstr ""

#: src/conf.c:837
#, c-format
msgid "cannot evaluate '%s': %s"
msgstr ""

#: src/conf.c:844
#, c-format
msgid "cannot read output of '%s'"
msgstr ""

#: src/conf.c:858
#, c-format
msgid "output of '%s' is longer than %d characters"
msgstr ""

#: src/conf.c:904 src/conf.c:911 src/conf.c:924 src/tls.c:891
#, c-format
msgid "%s: %s"
msgstr ""

#: src/conf.c:1070 src/stream.c:63
#, c-format
msgid "input error"
msgstr ""

#: src/conf.c:1092
#, c-format
msgid "line longer than %d characters"
msgstr ""

#: src/conf.c:1143 src/conf.c:1268
#, c-format
msgid "line %d: missing account name"
msgstr ""

#: src/conf.c:1148
#, c-format
msgid "line %d: account %s not (yet) defined"
msgstr ""

#: src/conf.c:1240 src/conf.c:2037 src/conf.c:2054
#, c-format
msgid "line %d: command %s does not take an argument"
msgstr ""

#: src/conf.c:1276
#, c-format
msgid "line %d: an account name must not contain colons or commas"
msgstr ""

#: src/conf.c:1286
#, c-format
msgid "line %d: account %s was already defined"
msgstr ""

#: src/conf.c:1311 src/conf.c:1330 src/conf.c:1357 src/conf.c:1386
#: src/conf.c:1672 src/conf.c:1707
#, c-format
msgid "line %d: command %s needs an argument"
msgstr ""

#: src/conf.c:1341 src/conf.c:1373 src/conf.c:1404 src/conf.c:1443
#: src/conf.c:1488 src/conf.c:1508 src/conf.c:1593 src/conf.c:1614
#: src/conf.c:1633 src/conf.c:1694 src/conf.c:1725 src/conf.c:1775
#: src/conf.c:1822 src/conf.c:1847 src/conf.c:1867 src/conf.c:1887
#: src/conf.c:1907 src/conf.c:1954 src/conf.c:1975 src/conf.c:1996
#: src/conf.c:2024
#, c-format
msgid "line %d: invalid argument %s for command %s"
msgstr ""

#: src/conf.c:2070
#, c-format
msgid "line %d: unknown command %s"
msgstr ""

#: src/conf.c:2122
#, c-format
msgid "contains secrets and therefore must be owned by you"
msgstr ""

#: src/conf.c:2128
#, c-format
msgid ""
"contains secrets and therefore must have no more than user read/write "
"permissions"
msgstr ""

#: src/smtp.c:209
#, c-format
msgid "the server sent an empty reply"
msgstr ""

#: src/smtp.c:213
#, c-format
msgid "the server sent an invalid reply"
msgstr ""

#: src/smtp.c:237
#, c-format
msgid ""
"Rejecting server reply that is longer than %d lines. Increase SMTP_MAXLINES."
msgstr ""

#: src/smtp.c:324
#, c-format
msgid ""
"Cannot send command because it is longer than %d characters. Increase "
"SMTP_MAXCMDLEN."
msgstr ""

#: src/smtp.c:356
#, c-format
msgid "cannot get initial OK message from server"
msgstr ""

#: src/smtp.c:418
#, c-format
msgid "SMTP server does not accept EHLO or HELO commands"
msgstr ""

#: src/smtp.c:441 src/smtp.c:591 src/smtp.c:596 src/smtp.c:669 src/smtp.c:717
#: src/smtp.c:807 src/smtp.c:898 src/smtp.c:2043
#, c-format
msgid "command %s failed"
msgstr ""

#: src/smtp.c:674 src/smtp.c:738 src/smtp.c:759 src/smtp.c:856 src/smtp.c:919
#: src/smtp.c:993 src/smtp.c:1319 src/smtp.c:1361 src/smtp.c:1398
#, c-format
msgid "authentication failed (method %s)"
msgstr ""

#: src/smtp.c:818
#, c-format
msgid "authentication method CRAM-MD5: server sent invalid challenge"
msgstr ""

#: src/smtp.c:1106 src/smtp.c:1419
#, c-format
msgid "the server does not support authentication method %s"
msgstr ""

#: src/smtp.c:1112 src/smtp.c:1230 src/smtp.c:1273
#, c-format
msgid "GNU SASL: %s"
msgstr ""

#: src/smtp.c:1119
#, c-format
msgid "GNU SASL: authentication method %s not supported"
msgstr ""

#: src/smtp.c:1178 src/smtp.c:1450
#, c-format
msgid "cannot use a secure authentication method"
msgstr ""

#: src/smtp.c:1184 src/smtp.c:1456
#, c-format
msgid "cannot find a usable authentication method"
msgstr ""

#: src/smtp.c:1197 src/smtp.c:1468
#, c-format
msgid "authentication method %s needs a user name"
msgstr ""

#: src/smtp.c:1210 src/smtp.c:1478
#, c-format
msgid "authentication method %s needs a password"
msgstr ""

#: src/smtp.c:1377
#, c-format
msgid "authentication failed: %s (method %s)"
msgstr ""

#: src/smtp.c:1509
#, c-format
msgid "authentication method %s not supported"
msgstr ""

#: src/smtp.c:1623
#, c-format
msgid "envelope from address %s not accepted by the server"
msgstr ""

#: src/smtp.c:1642
#, c-format
msgid "recipient address %s not accepted by the server"
msgstr ""

#: src/smtp.c:1661
#, c-format
msgid "the server does not accept mail data"
msgstr ""

#: src/smtp.c:1901
#, c-format
msgid "the server did not accept the mail"
msgstr ""

#: src/smtp.c:1965
#, c-format
msgid "the server refuses to send the mail to %s"
msgstr ""

#: src/smtp.c:2029
#, c-format
msgid "the server is unable to fulfill the request"
msgstr ""

#: src/smtp.c:2037
#, c-format
msgid "invalid argument for Remote Message Queue Starting"
msgstr ""

#: src/net.c:94
msgid "not enough memory"
msgstr ""

#: src/net.c:97 src/net.c:208 src/net.c:255 src/net.c:767 src/net.c:894
#: src/tls.c:1641 src/tls.c:1706 src/tls.c:1749 src/tls.c:1836 src/tls.c:1875
#, c-format
msgid "operation aborted"
msgstr ""

#: src/net.c:100 src/net.c:684
msgid "invalid argument"
msgstr ""

#: src/net.c:103
msgid "class type not found"
msgstr ""

#: src/net.c:106
msgid "the network subsystem has failed"
msgstr ""

#: src/net.c:109
msgid "host not found (authoritative)"
msgstr ""

#: src/net.c:112
msgid "host not found (nonauthoritative) or server failure"
msgstr ""

#: src/net.c:115
msgid "nonrecoverable error"
msgstr ""

#: src/net.c:118
msgid "valid name, but no data record of requested type"
msgstr ""

#: src/net.c:121
msgid "address family not supported"
msgstr ""

#: src/net.c:124
msgid "no socket descriptors available"
msgstr ""

#: src/net.c:127
msgid "no buffer space available"
msgstr ""

#: src/net.c:130
msgid "protocol not supported"
msgstr ""

#: src/net.c:133
msgid "wrong protocol type for this socket"
msgstr ""

#: src/net.c:136
msgid "socket type is not supported in this address family"
msgstr ""

#: src/net.c:139
msgid "remote address is not valid"
msgstr ""

#: src/net.c:142 src/net.c:636
msgid "connection refused"
msgstr ""

#: src/net.c:145 src/net.c:630
msgid "network unreachable"
msgstr ""

#: src/net.c:148
msgid "timeout"
msgstr ""

#: src/net.c:151
msgid "socket not connected"
msgstr ""

#: src/net.c:154
msgid "the socket was shut down"
msgstr ""

#: src/net.c:157 src/net.c:633
msgid "host unreachable"
msgstr ""

#: src/net.c:160
msgid "connection reset by peer"
msgstr ""

#: src/net.c:163
msgid "the underlying network subsystem is not ready"
msgstr ""

#: src/net.c:166
msgid "the requested version is not available"
msgstr ""

#: src/net.c:169
msgid "a blocking operation is in progress"
msgstr ""

#: src/net.c:172
msgid "limit on the number of tasks has been reached"
msgstr ""

#: src/net.c:175
msgid "invalid request"
msgstr ""

#: src/net.c:178 src/net.c:648 src/tls.c:1533 src/tls.c:1557 src/tls.c:1852
msgid "unknown error"
msgstr ""

#: src/net.c:197 src/net.c:202 src/net.c:212 src/net.c:217
#, c-format
msgid "network read error: %s"
msgstr ""

#: src/net.c:198 src/net.c:213 src/net.c:245 src/net.c:260 src/tls.c:1552
msgid "the operation timed out"
msgstr ""

#: src/net.c:244 src/net.c:249 src/net.c:259 src/net.c:264
#, c-format
msgid "network write error: %s"
msgstr ""

#: src/net.c:528 src/net.c:558 src/net.c:593 src/net.c:624 src/net.c:627
#: src/net.c:630 src/net.c:633 src/net.c:636 src/net.c:639 src/net.c:642
#: src/net.c:645 src/net.c:648
#, c-format
msgid "proxy failure: %s"
msgstr ""

#: src/net.c:528
msgid "host name too long"
msgstr ""

#: src/net.c:542 src/net.c:576 src/net.c:1007
#, c-format
msgid "network write error"
msgstr ""

#: src/net.c:552 src/net.c:586 src/net.c:615
#, c-format
msgid "network read error"
msgstr ""

#: src/net.c:558 src/net.c:593
msgid "unexpected reply"
msgstr ""

#: src/net.c:624
msgid "general server failure"
msgstr ""

#: src/net.c:627
msgid "connection not allowed"
msgstr ""

#: src/net.c:639
msgid "time-to-live expired"
msgstr ""

#: src/net.c:642
msgid "command not supported"
msgstr ""

#: src/net.c:645
msgid "address type not supported"
msgstr ""

#: src/net.c:676 src/net.c:683 src/net.c:698
#, c-format
msgid "cannot connect to %s: %s"
msgstr ""

#: src/net.c:689 src/net.c:862
#, c-format
msgid "cannot create socket: %s"
msgstr ""

#: src/net.c:762 src/net.c:771
#, c-format
msgid "cannot locate host %s: %s"
msgstr ""

#: src/net.c:873
#, c-format
msgid "cannot bind source ip %s: %s"
msgstr ""

#: src/net.c:889 src/net.c:902
#, c-format
msgid "cannot connect to %s, port %d: %s"
msgstr ""

#: src/tls.c:110
#, c-format
msgid ""
"no environment variables RANDFILE or HOME, or filename of rand file too long"
msgstr ""

#: src/tls.c:116
#, c-format
msgid "%s: input error"
msgstr ""

#: src/tls.c:138
#, c-format
msgid "random file + time + pseudo randomness is not enough, giving up"
msgstr ""

#: src/tls.c:381 src/tls.c:495
msgid "cannot get TLS certificate info"
msgstr ""

#: src/tls.c:386 src/tls.c:498 src/tls.c:770 src/tls.c:900
#, c-format
msgid "%s: no certificate was found"
msgstr ""

#: src/tls.c:391 src/tls.c:776 src/tls.c:913
#, c-format
msgid "%s: cannot initialize certificate structure"
msgstr ""

#: src/tls.c:397
#, c-format
msgid "%s: error parsing certificate"
msgstr ""

#: src/tls.c:407 src/tls.c:518 src/tls.c:793 src/tls.c:1022
#, c-format
msgid "%s: error getting SHA256 fingerprint"
msgstr ""

#: src/tls.c:415 src/tls.c:524 src/tls.c:812 src/tls.c:1040
#, c-format
msgid "%s: error getting SHA1 fingerprint"
msgstr ""

#: src/tls.c:421 src/tls.c:532
#, c-format
msgid "%s: cannot get activation time"
msgstr ""

#: src/tls.c:427 src/tls.c:542
#, c-format
msgid "%s: cannot get expiration time"
msgstr ""

#: src/tls.c:503 src/tls.c:1131
#, c-format
msgid "%s: cannot get certificate subject"
msgstr ""

#: src/tls.c:509
#, c-format
msgid "%s: cannot get certificate issuer"
msgstr ""

#: src/tls.c:611
msgid "Common Name"
msgstr ""

#: src/tls.c:611
msgid "Organization"
msgstr ""

#: src/tls.c:612
msgid "Organizational unit"
msgstr ""

#: src/tls.c:612
msgid "Locality"
msgstr ""

#: src/tls.c:612
msgid "State or Province"
msgstr ""

#: src/tls.c:613
msgid "Country"
msgstr ""

#: src/tls.c:620
#, c-format
msgid "TLS session parameters:\n"
msgstr ""

#: src/tls.c:622
msgid "not available"
msgstr ""

#: src/tls.c:627
#, c-format
msgid "TLS certificate information:\n"
msgstr ""

#: src/tls.c:628
msgid "Owner"
msgstr ""

#: src/tls.c:639
msgid "Issuer"
msgstr ""

#: src/tls.c:650
msgid "Validity"
msgstr ""

#: src/tls.c:652
msgid "Activation time"
msgstr ""

#: src/tls.c:654
msgid "Expiration time"
msgstr ""

#: src/tls.c:655
msgid "Fingerprints"
msgstr ""

#: src/tls.c:755 src/tls.c:998
msgid "TLS certificate verification failed"
msgstr ""

#: src/tls.c:759 src/tls.c:1002
msgid "TLS certificate check failed"
msgstr ""

#: src/tls.c:783 src/tls.c:920
#, c-format
msgid "%s: error parsing certificate %u of %u"
msgstr ""

#: src/tls.c:800 src/tls.c:819 src/tls.c:838 src/tls.c:1029 src/tls.c:1047
#: src/tls.c:1065
#, c-format
msgid "%s: the certificate fingerprint does not match"
msgstr ""

#: src/tls.c:831 src/tls.c:1058
#, c-format
msgid "%s: error getting MD5 fingerprint"
msgstr ""

#: src/tls.c:859
#, c-format
msgid "%s: the certificate type is not X509"
msgstr ""

#: src/tls.c:868
#, c-format
msgid "%s: the certificate has been revoked"
msgstr ""

#: src/tls.c:875
#, c-format
msgid "%s: the certificate hasn't got a known issuer"
msgstr ""

#: src/tls.c:881
#, c-format
msgid "%s: the certificate is not trusted"
msgstr ""

#: src/tls.c:935 src/tls.c:1169
#, c-format
msgid "%s: the certificate owner does not match hostname %s"
msgstr ""

#: src/tls.c:943
#, c-format
msgid "%s: cannot get activation time for certificate %u of %u"
msgstr ""

#: src/tls.c:951
#, c-format
msgid "%s: certificate %u of %u is not yet activated"
msgstr ""

#: src/tls.c:957
#, c-format
msgid "%s: cannot get expiration time for certificate %u of %u"
msgstr ""

#: src/tls.c:964
#, c-format
msgid "%s: certificate %u of %u has expired"
msgstr ""

#: src/tls.c:1008
#, c-format
msgid "%s: no certificate was sent"
msgstr ""

#: src/tls.c:1111
#, c-format
msgid "%s: certificate subject alternative name contains NUL"
msgstr ""

#: src/tls.c:1143
#, c-format
msgid "%s: cannot get certificate common name"
msgstr ""

#: src/tls.c:1152
#, c-format
msgid "%s: certificate common name contains NUL"
msgstr ""

#: src/tls.c:1229
#, c-format
msgid "cannot initialize TLS session: %s"
msgstr ""

#: src/tls.c:1242
#, c-format
msgid "error in priority string at position %d"
msgstr ""

#: src/tls.c:1245 src/tls.c:1252 src/tls.c:1388
#, c-format
msgid "cannot set priorities for TLS session: %s"
msgstr ""

#: src/tls.c:1263
#, c-format
msgid "cannot set default priority for TLS session: %s"
msgstr ""

#: src/tls.c:1276
#, c-format
msgid "cannot allocate certificate for TLS session: %s"
msgstr ""

#: src/tls.c:1287
#, c-format
msgid ""
"cannot set X509 key file %s and/or X509 cert file %s for TLS session: %s"
msgstr ""

#: src/tls.c:1307 src/tls.c:1444
#, c-format
msgid "cannot set X509 system trust for TLS session: %s"
msgstr ""

#: src/tls.c:1320
#, c-format
msgid "cannot set X509 trust file %s for TLS session: %s"
msgstr ""

#: src/tls.c:1333
#, c-format
msgid "cannot set X509 CRL file %s for TLS session: %s"
msgstr ""

#: src/tls.c:1360
#, c-format
msgid "cannot set credentials for TLS session: %s"
msgstr ""

#: src/tls.c:1380
#, c-format
msgid "cannot set minimum number of DH prime bits for TLS: %s"
msgstr ""

#: src/tls.c:1381 src/tls.c:1389 src/tls.c:1397
msgid "feature not yet implemented for OpenSSL"
msgstr ""

#: src/tls.c:1396
#, c-format
msgid "cannot load CRL file: %s"
msgstr ""

#: src/tls.c:1403
#, c-format
msgid "cannot set TLS method"
msgstr ""

#: src/tls.c:1408
#, c-format
msgid "cannot create TLS context: %s"
msgstr ""

#: src/tls.c:1419
#, c-format
msgid "cannot load key file %s: %s"
msgstr ""

#: src/tls.c:1427
#, c-format
msgid "cannot load certificate file %s: %s"
msgstr ""

#: src/tls.c:1455
#, c-format
msgid "cannot load trust file %s: %s"
msgstr ""

#: src/tls.c:1481
#, c-format
msgid "cannot create a TLS structure: %s"
msgstr ""

#: src/tls.c:1525
msgid "a protocol violating EOF occurred"
msgstr ""

#: src/tls.c:1543
msgid "the connection was closed unexpectedly"
msgstr ""

#: src/tls.c:1591
#, c-format
msgid "TLS handshake failed: %s"
msgstr ""

#: src/tls.c:1628
#, c-format
msgid "cannot set the file descriptor for TLS: %s"
msgstr ""

#: src/tls.c:1647
msgid "TLS handshake failed"
msgstr ""

#: src/tls.c:1710
#, c-format
msgid "cannot read from TLS connection: %s"
msgstr ""

#: src/tls.c:1754
msgid "cannot read from TLS connection"
msgstr ""

#: src/tls.c:1840 src/tls.c:1851
#, c-format
msgid "cannot write to TLS connection: %s"
msgstr ""

#: src/tls.c:1881
msgid "cannot write to TLS connection"
msgstr ""
